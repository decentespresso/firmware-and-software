#!/usr/bin/python3
import matplotlib

matplotlib.use("wxagg")
import matplotlib.pyplot as plt
from math import *
import autograd.numpy as np
from autograd import grad
from autograd import elementwise_grad as egrad
from scipy import integrate

import click


def clamp(x, lowerlimit, upperlimit):
    if (x < lowerlimit):
        x = lowerlimit
    if (x > upperlimit):
        x = upperlimit
    return x


def smootherstep(edge0, edge1, x, order=7):
    # Scale, and clamp x to 0..1 range
    x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
    # Evaluate polynomial

    if order == 7:
        return x * x * x * x * (35.0 - 20.0 * x * x * x + 70.0 * x * x - 84.0 * x)

    if order == 5:
        return x * x * x * (x * (x * 6.0 - 15.0) + 10.0)

    if order == 3:
        return x * x * (3.0 - 2.0 * x)


def make_lin1(baseradius, pistonthrow):
    rs = []
    drs = []
    for i in range(0, 90):
        rs.append(i)
        drs.append(1)
    for i in range(0, 90):
        rs.append(90 - i)
        drs.append(-1)
    for i in range(0, 90):
        rs.append(i)
        drs.append(1)
    for i in range(0, 90):
        rs.append(90 - i)
        drs.append(-1)

    rs = baseradius + np.array(rs) / 90.0
    drs = np.array(drs) / 90.0

    return rs, drs


def make_lin2(baseradius, pistonthrow):
    rs = []
    drs = []
    for i in range(0, 180):
        rs.append(i)
        drs.append(1)
    for i in range(0, 180):
        rs.append(180 - i)
        drs.append(-1)

    rs = baseradius + pistonthrow * np.array(rs) / 180.0
    drs = pistonthrow * np.array(drs) / 180.0

    return rs, drs


def make_rs_pt(angle, baseradius, pistonthrow, order):
    if angle >= 180:
        angle = 360.0 - angle
    return baseradius + smootherstep(0.0, 180.0, angle, order=order) * pistonthrow


def make_rs(angles, baseradius, pistonthrow, order):
    step = lambda x: make_rs_pt(x, baseradius, pistonthrow, order)
    vf = np.vectorize(step)
    rs = vf(angles)
    return rs


def make_lin3(angles, baseradius, pistonthrow, order):
    """
  Make a cam profile based on the idea of a linear cam that is 
  smoothed with Hermetian interpolation.

  For a constant width cam, the sum of the radius and the radius 180 degrees later
  must add to the constant. Being symmetrical around 180 degrees is one easy way to do this.

  For perfectly smooth flow, the flow has to sum to a constant with itself delayed by multiples of 90 degrees.
  Using a triangle shape (0 - 180 - 360 = 0 - 1 - 0) works, but produces a cam with sharp concave edges and instantaneous
  changes of direction.

  Smoothing with a Hermetian means we can hold three derivatives at zero at the ends of a stroke. So, zero velocity, acceleration, and jerk.
  This should make for a very smooth piston motion, and spreads forces out over time.

  But, the derivatives of the Hermitian curve don't sum with themselves to a constant, as they are not symmetric around 0.5.

  So, I cheat, and divide one by the sum of derivatives, providing a correction factor. I apply that to the derivatives, and then integrate
  to get the cam profile.

  """

    # Work out first cut Hermitian interpolated linear cam.
    rs = make_rs(angles, baseradius, pistonthrow, order)

    # Use the autograd library to produce an analytic derivative and
    # apply it to an array.
    lrs = lambda x: make_rs_pt(x, baseradius, pistonthrow, order)
    grad_rs = grad(lrs)
    v_drs = np.vectorize(grad_rs)
    drs = v_drs(angles)
    # for a,b in zip(rs, integrate.cumtrapz(drs, angles)+5.0):
    #  print(a,b)
    # drs = drs * np.max(rs-5.0)/np.max(integrate.cumtrapz(drs, initial=0.0))
    # for a,b in zip(rs, integrate.cumtrapz(drs, initial=0.0)+5.0):
    #   print(a,b)

    # The sum wobbles between 1 at the edges and around 0.85 in the middle
    # in a 90 degree block.
    # So calculate the inverse wobble.
    nd = len(angles) // 4  # Ninety degrees in the array
    fsum = np.max(drs) / (drs[0:nd] + drs[nd:nd + nd])
    fsum = np.concatenate([fsum, fsum, fsum, fsum])

    # Multiply by the inverse of the error to work out a derivative that would give us constant flow
    drs = drs * fsum

    # Now recalculate rs by just taking the cumulative sum of the derivatives (probably less than ideal, but seems to work
    # well enough, as there are a fair number of points, and the curve is very smooth)
    # rs = np.cumsum(drs)
    rs = integrate.cumtrapz(drs, angles, initial=0.0)
    rsmax = np.max(rs)
    rs = rs / rsmax
    rs = (rs * pistonthrow) + baseradius

    drs = (drs / rsmax) * pistonthrow  # Scale derivatives by the same amount we scaled the integral
    # R is no longer a perfect Hermetian, but the error above was mostly in the center, meaning the zero derivatives at the
    # edges part is still very much in force.

    # drs is now in mm/deg, which is not a great measurement
    # Change to mm/rev
    drs = drs * 360.0

    # for a,b in zip(rs, drs):
    #  print(a,b)
    return rs, drs


def wrap(arr):
    return np.concatenate([arr, [arr[0]]])


def uniqf(angles, values):
    last = values[0]

    reta = [angles[0]]
    retv = [values[0]]
    for a, v in zip(angles, values):
        if abs(v - last) > 0.1:
            reta.append(a)
            retv.append(v)
            last = v

    return np.array(reta), np.array(retv)


def smooth(angles, values):
    reta = [angles[0]]
    retv = [values[0]]

    lasta = angles[1]
    lastv = values[1]

    sumv = 0.0
    suma = 0.0
    count = 0
    for a, v in zip(angles[1:-1], values[1:-1]):
        if abs(v - lastv) < 0.000001:
            suma += a
            sumv += v
            count += 1
        else:
            reta.append(suma / count)
            retv.append(sumv / count)
            suma = a
            sumv = v
            count = 1
            lastv = v

    reta.append(angles[-1])
    retv.append(values[-1])
    return np.array(reta), np.array(retv)


def uniq(angles, values):
    fa, fv = uniqf(angles, values)
    ra, rv = uniqf(angles[::-1], values[::-1])
    fpairs = list(zip(fa, fv))
    rpairs = list(zip(ra, rv))
    pairs = sorted(fpairs + rpairs)
    a, v = zip(*pairs)
    return np.array(a), np.array(v)


def showcam(baseradius, pistonthrow, pistondiamm, order, figfilename="", csvfilename=""):
    fig = plt.figure(figsize=(45, 45), constrained_layout=False)
    fig.suptitle(
        "Cam shape that produces smooth flow. One piston cycle per cam revolution.\n Cam min radius: %.2fmm, Piston Throw %.2fmm, %0.2fmm Piston" % (
            baseradius, pistonthrow, pistondiamm), fontsize=20)
    ax1 = fig.add_subplot(411, projection='polar')  # There will be 3 subplots. 1st one across and down
    ax1.set_title("Cam shape\n ")

    pts = 3600
    scale = 360.0 / pts
    angles = np.array([x * scale for x in range(0, pts)])  # Plot one point for every degree
    rads = 2 * pi * angles / 360.0
    # rads = [2*pi*angle/360.0 for angle in angles]  # Convert to rads

    rs, drs = make_lin3(angles, baseradius, pistonthrow, order)

    # rs  = [ (baseradius - pistonthrow*0.5*sin(a)) for a in rads]  # r = base + wobble
    # drs = np.maximum(0, np.array([ pistonthrow * ( (sin(a/2)*sin(a/2) - 1/2) ) for a in rads]))  # r = base + wobble

    # rs  = [ (2*baseradius*x + pistonthrow*x - pistonthrow*sin(x)/2) for x in rads]
    # drs = np.maximum(0, np.array([ (pistonthrow*sin(x/2)*sin(x/2)) for x in rads]))  # r = base + wobble

    # rs  = [ (baseradius - pistonthrow*0.25*sin(a)) for a in rads]  # r = base + wobble
    # drs = np.maximum(0, np.array([ (-0.25*pistonthrow*cos(a)) for a in rads]))  # r = base + wobble

    # rs  = [ (baseradius + pistonthrow*sin(a/2)*sin(a/2)) for a in rads]  # r = base + wobble
    # drs = np.maximum(0, np.array([ (0.5*pistonthrow*sin(a)) for a in rads]))  # r = base + wobble

    # rs  = [ (baseradius + pistonthrow*sin(a)*sin(a)) for a in rads]  # r = base + wobble
    # drs = np.maximum(0, np.array([ (pistonthrow*sin(2*a)) for a in rads]))  # r = base + wobble

    plt.polar(rads, rs)  # plot in ax1

    ax2 = fig.add_subplot(412)  # Add 2nd subplot, 1 across, 2 down
    ax2.plot(angles, rs)
    ax2.grid(True)
    ax2.set_title("Piston position")
    ax2.set_xticks([0, 45, 90, 135, 180, 225, 270, 315, 360])

    widths = []
    widthangles = []
    deg180 = len(rs) // 2
    for i in range(0, deg180):
        w = rs[i] + rs[i + deg180]
        w = round(w * 1000.0) / 1000.0
        widths.append(w)  # Width is just the sum of radii 180 deg apart
        widthangles.append(180 * i / deg180)

    ax3 = fig.add_subplot(413)  # Add 3rd subplot of 3, 1 across 3 down
    ax3.plot(widthangles, widths)
    ax3.grid(True)
    ax3.set_xticks([0, 45, 90, 135, 180])
    ax3.set_title("Width of cam, starting at given angle, passing through origin/[mm]")
    if figfilename != "":
        plt.savefig(figfilename)

    ax4 = fig.add_subplot(414)  # Add 3rd subplot of 3, 1 across 3 down
    pistonrinm = (pistondiamm / 2.0) / 1000.0
    flows = drs * 0.001 * pi * pistonrinm * pistonrinm * 1E6

    flow = []
    deg90 = len(flows) // 4
    ld = len(flows)
    for i in range(0, len(flows)):
        flow.append(
            max(0, flows[i]) +
            max(0, flows[(i + deg90) % ld]) +
            max(0, flows[(i + deg90 * 2) % ld]) +
            max(0, flows[(i + deg90 * 3) % ld])
        )

    ax4.plot(angles, flow, linewidth=2, label="Total flow/[ml/s at 1 rev/s]")
    ax4.plot(angles, flows, alpha=0.25, linewidth=2.0)
    ax4.plot(angles, [flows[(i + deg90 * 1) % ld] for i in range(0, ld)], alpha=0.25, linewidth=2.0)
    ax4.plot(angles, [flows[(i + deg90 * 2) % ld] for i in range(0, ld)], alpha=0.25, linewidth=2.0)
    ax4.plot(angles, [flows[(i + deg90 * 3) % ld] for i in range(0, ld)], alpha=0.25, linewidth=2.0)
    ax4.grid(True)
    ax4.set_xticks([0, 45, 90, 135, 180, 225, 270, 315, 360])
    ax4.set_title("Flow/[ml/s at 1 rev/s] (Sum: %0.4f ml/s at 1 rev/s)" % (np.max(flow)))
    if figfilename != "":
        plt.savefig(figfilename)

    if csvfilename != "":
        f = open(csvfilename, "w")
        f.write("Angle/[Deg],R/[mm]\n")
        for a, r in zip(angles, rs):
            f.write("%f,%s\n" % (a, r))

        f.close()

        f = open("cart_" + csvfilename, "w")
        f.write("x/[mm],y/[mm]\n")
        for a, r in zip(rads, rs):
            f.write("%f,%s\n" % (r * cos(a), r * sin(a)))

        f.close()

    return fig, rs, drs, rads, angles


def shavecamslow(rs, drs, rads, flatcamwidth):
    """
  Trim a cam profile so that it provides the requested output with a flat cam follower
  """
    yrange = flatcamwidth / 2.0
    l = len(rs)
    trimmed = np.array(rs)
    sinarr = np.sin(rads)
    cosarr = np.cos(rads)

    follow = np.zeros_like(rs)
    for j in range(l):
        xs = trimmed * cosarr
        ys = trimmed * sinarr
        # Identify points that would touch the follower
        r = rs[0]
        for i, x, y in zip(range(l), xs, ys):
            if x >= r:
                # Need to trim if would touch the follower
                # above or below the contact point
                if abs(y) <= yrange:
                    s = sqrt(r * r + y * y)
                    trimmed[i] = min(trimmed[i], s)  # Calc new r

        rs = np.roll(rs, -1)
        trimmed = np.roll(trimmed, -1)

    return trimmed


def shavecam(rs, drs, rads, flatcamwidth):
    """
  Trim a cam profile so that it provides the requested output with a flat cam follower
  """
    yrange = flatcamwidth / 2.0
    l = len(rs)
    trimmed = np.array(rs)
    sinarr = np.sin(rads)
    cosarr = np.cos(rads)

    follow = np.zeros_like(rs)
    for j in range(l):
        xs = trimmed * cosarr
        ys = trimmed * sinarr

        sxs = np.argsort(xs,
                         kind='mergesort')  # Produce a list of indices that sort xs. Timsort ('mergesort') is ultra fast for almost sorted inputs
        # Identify points that would touch the follower
        r = rs[0]
        inspt = np.searchsorted(xs, r, sorter=sxs)  # Find first x >= r
        for i in sxs[inspt:]:  # Step through everything larger than r
            y = ys[i]
            if abs(y) <= yrange:
                s = sqrt(r * r + y * y)
                trimmed[i] = min(trimmed[i], s)  # Calc new r

        # for i,x,y in zip(range(l), xs, ys):
        #   if x >= r:
        #     # Need to trim if would touch the follower
        #     # above or below the contact point
        #     if abs(y) <= yrange:
        #       s = sqrt(r*r + y*y)
        #       trimmed[i] = min(trimmed[i], s) # Calc new r

        rs = np.roll(rs, -1)
        trimmed = np.roll(trimmed, -1)

    return trimmed


def followcam(rs, drs, rads, flatcamwidth):
    """
  trim a cam profile so that it provides the requested output with a flat cam follower
  """
    yrange = flatcamwidth / 2.0
    l = len(rs)
    trimmed = np.array(rs)
    sinarr = np.sin(rads)
    cosarr = np.cos(rads)

    follow = np.zeros_like(rs)
    contactpts = np.zeros_like(rs)
    for j in range(l):
        xs = rs * cosarr
        ys = rs * sinarr
        xmaxi = np.argmax(xs)
        # Identify points that would touch the follower
        # Try the fast way first
        if abs(ys[xmaxi]) <= yrange:
            follow[j] = xs[xmaxi]
            contactpts[j] = atan2(ys[xmaxi], xs[xmaxi])
        else:
            asd
            # Else try the slow way
            for i, x, y in zip(range(l), xs, ys):
                if abs(y) <= yrange:
                    if follow[j] < x:
                        follow[j] = x
                        contactpts[j] = atan2(y, x)

        rs = np.roll(rs, -1)

    return follow, contactpts


def showshaved(rs, drs, rads, angles, flatcamwidth, pistondiamm, baseradius, pistonthrow, csvfilename=""):
    pistonrinm = (pistondiamm / 2.0) / 1000.0

    totspy = 5  # Total subplots across
    totspx = 1  # Total subplots down

    trimmed = shavecam(rs, drs, rads, flatcamwidth)

    fig = plt.figure(figsize=(45, 45), constrained_layout=False)
    fig.suptitle(
        "Cam vs trimmed cam\nCam min radius: %.2fmm, Piston Throw %.2fmm, %0.2fmm Piston\nTrimmed cam has constant width between two parallel surfaces" % (
            baseradius, pistonthrow, pistondiamm), fontsize=20)
    ax1 = fig.add_subplot(totspy, totspx, 1, projection='polar')  # There will be 3 subplots. 1st one across and down
    ax1.set_title("Cam shapes\n ")

    plt.polar(wrap(rads), wrap(rs), color="red", alpha=0.5, label="Original")  # plot in ax1
    plt.polar(wrap(rads), wrap(trimmed), color="blue", alpha=0.5, label="Trimmed")  # plot in ax1

    followed, contactpts = followcam(trimmed, drs, rads, flatcamwidth)
    plt.polar(wrap(rads), wrap(followed), color="green", alpha=0.5, label="Followed")
    ax1.legend(loc='best')

    ax2 = fig.add_subplot(totspy, totspx, 2)
    ax2.plot(angles, rs, color="red", alpha=0.5, label="Original")
    ax2.plot(angles, followed, color="green", alpha=0.5, label="Followed")

    ax2.grid(True)
    ax2.set_title("Piston position")
    ax2.set_xticks([0, 45, 90, 135, 180, 225, 270, 315, 360])
    ax2.legend()

    # widths = []
    # widthangles = []
    # deg180 = len(trimmed)//2
    # for i in range(0,deg180):
    #   w = trimmed[i]+trimmed[i+deg180]
    #   w = round(w*1000.0)/1000.0
    #   widths.append(w)  # Width is just the sum of radii 180 deg apart
    #   widthangles.append(180*i/deg180)

    ax3 = fig.add_subplot(totspy, totspx, 3)
    a, v = smooth(angles, contactpts * 360 / (2 * pi))
    ax3.plot(a, v)

    # ax3.plot(angles, contactpts*360/(2*pi))
    ax3.grid(True)
    ax3.set_xticks([0, 45, 90, 135, 180, 225, 270, 315, 360])
    ax3.set_title("Angle of contact point, clockwise-spinning cam")

    flows = drs * 0.001 * pi * pistonrinm * pistonrinm * 1E6
    rpm = (12.0 / np.max(flows)) * 60.0
    pressure = 15.0
    area = pi * (pistonrinm ** 2)
    force = area * (pressure * 100000)
    omega = 2 * pi * rpm / 60.0  # Rotation rate in rads/sec

    ax4 = fig.add_subplot(totspy, totspx, 4)
    forces = force / np.cos(contactpts)
    a, v = smooth(angles, forces)
    ax4.plot(a, v)
    ax4.grid(True)
    ax4.set_xticks([0, 45, 90, 135, 180, 225, 270, 315, 360])
    ax4.set_title("Force at contact point, assuming 15 bar and %0.2fmm piston/[N]" % (pistondiamm,))

    ax5 = fig.add_subplot(totspy, totspx, 5)
    forces = 100.0 / np.cos(contactpts)

    # power = forces*(drs*np.cos(contactpts))*omega # F.v

    power = force * (drs / 1000.0) * rpm / 60.0  # F.v
    torque = power / omega

    iotorque = np.array(torque)
    avet = 0
    for i in range(len(iotorque)):
        t = iotorque[i]
        if t < 0.0:
            t = t * 0.5 / pressure

        iotorque[i] = abs(t)
        avet += abs(t)

    deg90 = len(iotorque) // 4
    sumiot = iotorque + np.roll(iotorque, deg90) + np.roll(iotorque, deg90 * 2) + np.roll(iotorque, deg90 * 3)
    avet = 4 * avet / len(iotorque)
    print(max(power), max(power) / omega)
    # Torque is Power/omega
    a, v = smooth(angles, iotorque)
    ax5.plot(angles, iotorque, label="Piston 1")
    ax5.plot(angles, np.roll(iotorque, deg90 * 1), label="Piston 2")
    ax5.plot(angles, np.roll(iotorque, deg90 * 2), label="Piston 3")
    ax5.plot(angles, np.roll(iotorque, deg90 * 3), label="Piston 4")

    ax5.plot([0, 360], [avet, avet], label="Sum torque %0.4fNm" % (avet,))
    ax5.grid(True)
    ax5.set_xticks([0, 45, 90, 135, 180, 225, 270, 315, 360])
    ax5.set_title("Torque required, assuming 15 bar and %.2fmm piston, 0.5bar lift, %.2f RPM" % (pistondiamm, rpm))
    ax5.legend()

    fig.savefig("Trimmed.pdf")

    if csvfilename != "":
        f = open(csvfilename, "w")
        f.write("Angle/[Deg],R/[mm]\n")
        for a, r in zip(angles, trimmed):
            f.write("%f,%s\n" % (a, r))

        f.close()

        f = open("cart_" + csvfilename, "w")
        f.write("x/[mm],y/[mm]\n")
        for a, r in zip(rads, trimmed):
            f.write("%f,%s\n" % (r * cos(a), r * sin(a)))

        f.close()


@click.command()
@click.argument("name")
@click.argument("baserad", default=6)
@click.argument("throw", default=3)
@click.argument("pdia", default=10)
@click.option("--derivs", default=3, show_default=1, help="Number of derivatives to hold at zero at end of stroke. 3,2, or 1")
def main(name, baserad, throw, pdia, derivs):
    """
    Write a PDF and CSV containing cam information.\n
        NAME    : Base name of the output files\n
        BASERAD : Base radius of cam [mm] [default: 6]\n
        THROW   : Piston throw [mm] [default: 3]\n
        PDIA    : Piston diameter [mm] [default: 10]\n
    """

    # @click.argument("--baserad",  default=6, help="Base radius of cam [mm]")
    # @click.argument("--name",  help="Base filename of files to generate. eg. Cam_inline")
    # @click.argument("--throw", default = 3, help="Piston throw [mm]")
    # @click.argument("--pdia",  default = 10, help="Piston diameter [mm]")

    # showcam(0.5, 1)
    # showcam(1.0, 1.0, figfilename="Cam_1_1.pdf", csvfilename="Cam_1_1.csv")

    # base = 6
    # throw = 3
    # pdia = 10
    order = (derivs*2)+1
    flatcamwidth = (baserad * 2.0 + throw)
    f, rs, drs, rads, angles = showcam(baserad, throw, pdia, order, figfilename="%s_%d_%d.pdf" % (name, baserad, throw))
    showshaved(rs, drs, rads, angles, flatcamwidth, pdia, baserad, throw,
               csvfilename="%s_trimmed_%d_%d.csv" % (name, baserad, throw))
    plt.show()


if __name__ == '__main__':
    main()
